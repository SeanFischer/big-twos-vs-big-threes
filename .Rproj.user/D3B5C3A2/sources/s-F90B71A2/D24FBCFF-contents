library(nbastatR)
library(googlesheets4)
library(dplyr)
library(reldist)
library(ggplot2)
library(ggimage)
library(mgcv)
library(mgcViz)

salary_info <- read_sheet("https://docs.google.com/spreadsheets/d/1Y4CyCfGb8zew9iSuXmhiifnpHCGSXb7LQ1VHaQVEWhk/edit#gid=0")
salary_cap_max <- read.csv('data/raw_salary_cap.csv')
team_logos <- read_sheet("https://docs.google.com/spreadsheets/d/1Zqhx-8OyMbJbPSYLh6b_jnT2U6iMi2gH1fr1kGV6c7E/edit#gid=0")

salary_info <-
  salary_info %>%
  inner_join(salary_cap_max, by = c("year" = "year")) %>%
  mutate(cap_share = salary / salary_cap) %>%
  select(-salary_cap, luxury_tax)

gini_salary <-
  salary_info %>%
  group_by(year, team) %>%
  summarise(cap_share = sum(cap_share),
            G = gini(salary),
            total_salary = sum(salary)) %>%
  ungroup() %>%
  inner_join(salary_cap_max, by = c("year" = "year")) %>%
  mutate(over_tax = ifelse(total_salary > luxury_tax, 1, 0)) %>%
  group_by(team) %>%
  arrange(year) %>%
  mutate(tax_lag = lag(over_tax)) %>%
  ungroup()

# team_results <- teams_annual_stats(all_active_teams = TRUE,
#                                    modes = c("Totals"),
#                                    season_types = c("Regular Season", 
#                                                     "Playoffs"),
#                                    nest_data = F)

load("data/team_results.RData")

# teams_details(all_teams = TRUE)
# rm(dataTeamAwardsChampionships, dataTeamAwardsConf, dataTeamAwardsDiv,
#             dataTeamHistory, dataTeamHistory, dataTeamHof, dataTeamRetired,
#             dataTeamSocialSites, df_dict_nba_teams, df_dict_nba_teams_history)

load("data/dataTeamBackground.RData")

team_results_2 <-
  team_results %>%
  filter(slugSeason %in% unique(gini_salary$year)) %>%
  inner_join(dataTeamBackground %>% dplyr::select(idTeam, slugTeam),
             by = c("idTeam" = "idTeam")) %>%
  inner_join(gini_salary, by = c("slugSeason" = "year",
                                 "slugTeam" = "team")) %>%
  mutate(cba_11 = ifelse(slugSeason %in% c("2011-12",
                                            "2012-13",
                                            "2013-14",
                                            "2014-15",
                                            "2016-17"), 
                          1,0),
         cba_17 = ifelse(slugSeason %in% c("2017-18",
                                           "2018-19"),
                         1, 0),
         champ_share = countWinsPlayoffs / 16) %>%
  group_by(slugTeam) %>%
  arrange(slugSeason) %>%
  mutate(pctWins_lag = lag(pctWins),
         champ_share_lag = lag(champ_share)) %>%
  ungroup() %>%
  inner_join(team_logos, by = c("slugSeason" = "year",
                                "slugTeam" = "team"))

ggplot(team_results_2, aes(x = G, y = pctWins)) +
  geom_hline(yintercept = 0.5, lty = 2, color = "grey50") +
  # geom_point(aes(color = slugTeam)) +
  geom_image(aes(image = url), size = 0.05) + 
  geom_smooth() +
  xlab("Gini Index") +
  ylab("Win %") +
  scale_color_discrete("Team") +
  fischeR::theme_saf_no_font()

ggplot(team_results_2, aes(x = cap_share, y = pctWins)) +
  geom_hline(yintercept = 0.5, lty = 2, color = "grey50") +
  geom_point(aes(color = slugTeam)) +
  geom_smooth() +
  xlab("Share of the Cap") +
  ylab("Win %") +
  scale_color_discrete("Team") +
  fischeR::theme_saf_no_font()

ggplot(team_results_2, aes(x = G, y = champ_share)) +
  geom_hline(yintercept = 0.5, lty = 2, color = "grey50") +
  geom_point(aes(color = slugTeam)) +
  geom_smooth() +
  xlab("Gini Index") +
  ylab("Playoff Wins out of 16") +
  scale_color_discrete("Team") +
  fischeR::theme_saf_no_font()

ggplot(team_results_2, aes(x = cap_share, y = champ_share)) +
  geom_hline(yintercept = 0.5, lty = 2, color = "grey50") +
  geom_point(aes(color = slugTeam)) +
  geom_smooth() +
  xlab("Share of the Cap") +
  ylab("Playoff Wins out of 16") +
  scale_color_discrete("Team") +
  fischeR::theme_saf_no_font()

m0 <- lm(pctWins ~ poly(G, 5) + poly(cap_share, 3) + cba_11 + cba_17 + over_tax,
         data = team_results_2)
summary(m0)

m1 <- gam(pctWins ~ s(G) + s(cap_share) + cba_11 + cba_17 + over_tax + 
            tax_lag, 
          data = team_results_2)
summary(m1)
p_m1 <- getViz(m1)
# p_m1_1 <- 
  plot(sm(p_m1, 1)) +
  geom_hline(yintercept = 0, lty = 4, color = "grey50") +
  l_fitLine(colour = "red") + l_rug(mapping = aes(x=x, y=y), alpha = 0.8) +
  l_ciLine(mul = 5, colour = "blue", linetype = 2) + 
  l_points(shape = 19, size = 1, alpha = 0.1) + 
  fischeR::theme_saf_no_font()

# p_m1_2 <- 
  plot(sm(p_m1, 2)) +
  geom_hline(yintercept = 0, lty = 4, color = "grey50") +
  l_fitLine(colour = "red") + l_rug(mapping = aes(x=x, y=y), alpha = 0.8) +
  l_ciLine(mul = 5, colour = "blue", linetype = 2) + 
  l_points(shape = 19, size = 1, alpha = 0.1) + 
  fischeR::theme_saf_no_font()

m2 <- lm(champ_share ~ poly(G, 3) + poly(cap_share, 5) + cba_11 + cba_17 + over_tax,
         data = team_results_2)
summary(m2)

m3 <- gam(champ_share ~ s(G) + s(cap_share) + cba_11 + cba_17 + over_tax,
          data = team_results_2)
summary(m3)
p_m3 <- getViz(m3)
# p_m3_1 <- 
  plot(sm(p_m3, 1)) +
  geom_hline(yintercept = 0, lty = 4, color = "grey50") +
  l_fitLine(colour = "red") + l_rug(mapping = aes(x=x, y=y), alpha = 0.8) +
  l_ciLine(mul = 5, colour = "blue", linetype = 2) + 
  l_points(shape = 19, size = 1, alpha = 0.1) + 
  fischeR::theme_saf_no_font()

# p_m3_2 <- 
  plot(sm(p_m3, 2)) +
  geom_hline(yintercept = 0, lty = 4, color = "grey50") +
  l_fitLine(colour = "red") + l_rug(mapping = aes(x=x, y=y), alpha = 0.8) +
  l_ciLine(mul = 5, colour = "blue", linetype = 2) + 
  l_points(shape = 19, size = 1, alpha = 0.1) + 
  fischeR::theme_saf_no_font()
